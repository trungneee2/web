<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="style.css">
        <title>Document</title>
    </head>
    <style>
    body {
        display: flex;
        justify-content: center;
    }

    .wrapper {
        border: 1px solid #41719c;
        padding: 20px 80px;
        display: inline-block;
    }

    label {
        background-color: #5b9bd2;
        color: white;
        width: 70px;
        padding-left: 10px;
        padding-top: 10px;
        padding-bottom: 10px;
        padding-right: 10px;
        border: 1px solid #41719c;
        margin-bottom: 10px;
        display: inline-block;
    }

    input {
        width: auto;
        line-height: 37px;
        margin-left: 10px;
        border: 1px solid #41719c;
    }

    select{
        width: 150px;
        height: 40px;
        line-height: 50px;
        margin-left: 10px;
        border: 1px solid #41719c;
    }

    button {
        width: auto;
        background-color: #70AD47;
        color: white;
        margin-left: 90px;
        border: 1px solid #41719c;
        padding: 15px 32px;
        border-radius: 7px;
        margin-top: 21px;
    }

    </style>
    <body>
        <div class = "wrapper">
            <form action="">

                <div class = "FullName">
                    <label >Họ và tên</label>
                    <input type="text">
                </div>

                <div class = "Gender">
                    <label >Giới tính</label>
                    <?php
                    $gender = array("Nam", "Nữ");
                    for ($x = 0; $x < count($gender); $x++){
                        print "<input type =\"radio\" name = \"gender\" value = \"Nam\"> $gender[$x]";
                    }
                    ?>
                </div>

                <div class = "khoa">
                <label >Phân khoa</label>
                <select name="khoa">
                    <?php
                    $khoa = array("" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
                    foreach($khoa as $x => $val){
                        print "<option value=\"$x\">$val</option>";
                    }
                    ?>
                </select>
            </div>
            </form>
            <Button>Đăng ký</Button>
        </div>
    </body>
</html>
