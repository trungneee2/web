<?php
$gender = array(0 => 'Nam', 1=> 'Nữ');
$khoa = array("" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
?>
<?php
    $error = "";
    session_start();
    function checkDateFormat($string) {
        $date_splitted = explode("/", $string);
        $date = "";
        if (count($date_splitted) == 3){
            $date = $date_splitted[1]."/".$date_splitted[0]."/".$date_splitted[2];
        } else {
            return 0;
        }
        if (strtotime($date)) {
            return 1;
        }
        return 0;
    }
    $data = array();

    if ($_SERVER["REQUEST_METHOD"] == "POST"){

        $birthday = isset($_POST['birthday']) ? trim( $_POST['birthday']):"";
        $is_correct_date_format = checkDateFormat($birthday);

        if (!isset($_POST['name']) || $_POST['name'] == null) {
            $error .= "<p>
            <font face=\"Arial\" style=\"text-align: center;  width: 100px;  color: red; margin-left: 80px;\">
                Hãy nhập tên.</font></p>";
        }
        
        if (!isset($_POST['gender']) || $_POST['gender'] == null) {
            $error .= "<p>
            <font face=\"Arial\" style=\"text-align: center;  width: 100px;  color: red; margin-left: 80px;\">
                Hãy chọn giới tính.</font></p>";
        }
        
        if (!isset($_POST['khoa']) || $_POST['khoa'] == null) {
            $error .= "<p>
            <font face=\"Arial\" style=\"text-align: center;  width: 100px;  color: red; margin-left: 80px;\">
                Hãy chọn phân khoa.</font></p>";
        }

        if (!isset($_POST['birthday']) || $_POST['birthday'] == null) {
            $error .= "<p>
            <font face=\"Arial\" style=\"text-align: center;  width: 100px;  color: red; margin-left: 80px;\">
                Hãy nhập ngày sinh.</font></p>";
        }
        if ($is_correct_date_format==0) {
            $error .= "<p>
            <font face=\"Arial\" style=\"text-align: center;  width: 100px;  color: red; margin-left: 80px;\">
                Hãy nhập ngày sinh đúng định dạng.</font></p>";
        }
        if(isset($_POST['name']) && isset($_POST['gender']) && isset($_POST['khoa']) && isset($_POST['birthday']) && $is_correct_date_format==1){
            header("location: confirm.php"); 
        }

        $_SESSION = $_POST;
        $_FILES = $_POST['image'];
    }

    $_SESSION['gender_array'] = $gender;

    $_SESSION['khoa_array'] = $khoa;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
    <link rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
</head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<style>
body {
    display: flex;
    justify-content: center;
}

.wrapper {
    border: 1px solid #41719c;
    padding-top: 40px;
    padding-right: 100px;
    padding-bottom: 15px;
    padding-left: 25px;
    display: inline-block;
}

.required:after {
    content:"*";
    color: red;
}

label {
    background-color: #70AD47;
    color: white;
    width: 80px;
    padding-left: 10px;
    padding-top: 10px;
    padding-bottom: 10px;
    padding-right: 10px;
    border: 1px solid #41719c;
    margin-bottom: 10px;
    display: inline-block;
}

input[type=text2] {
    width: 300px;
    line-height: 37px;
    margin-left: 10px;
    border: 1px solid #41719c;
}

select{
    width: 150px;
    height: 40px;
    line-height: 50px;
    margin-left: 10px;
    border: 1px solid #41719c;
}

input[type=text1]{
    width: 144px;
    line-height: 37px;
    margin-left: 10px;
    border: 1px solid #41719c;
}

input[type=radio]{
    width: auto;
    line-height: 37px;
    margin-left: 10px;
    border: 1px solid #70AD47;
}

input[type=file]{
    width: auto;
    line-height: 37px;
    margin-left: 10px;
}

input[type = submit] {
    width: 120px;
    height: 45px;
    background-color: #70AD47;
    color: white;
    margin-left: 190px;
    border: 1px solid #41719c;
    padding: 5px 25px;
    border-radius: 7px;
    margin-top: 15px;
}

.image_input {
    display: flex;
    align-items: center;
}

</style>
<body>

    <script type="text/javascript">
        function readURL(input) {
            console.log(input.files[0]);
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    $('#blah').css("display","block");
                    reader.onload = function (e) {
                        $('#blah')
                            .attr('src', e.target.result);
                        
                            $('.img_encode').val(e.target.result);
                    };

                    reader.readAsDataURL(input.files[0]);
                }else{
                    $('#blah').css("display","none");
                }
            }
    </script>
    <div class = "wrapper">
    <form action="" method="POST" enctype="multipart/form-data">
        <div class="error">
            <?php
            echo $error
            ?>
        </div>
        <div class = "name">
            <label class = "required">Họ và tên</label>
            <input type="text2" name = "name">
        </div>

        <div class = "Gender">
            <label class = "required">Giới tính</label>
            <?php 
                $gender = array(0 => 'Nam', 1=> 'Nữ');
                for ($i = 0; $i < count($gender); $i++){
                    echo "<input type=\"radio\" name = \"gender\"  value=\"$i\">{$gender[$i]}";
                }
            ?>
        </div>

        <div class = "khoa">
            <label class = "required">Phân khoa</label>
            <select name="khoa">
                <?php
                $khoa = array("" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
                foreach($khoa as $x => $val){
                    echo "<option value=\"$x\">$val</option>";
                }
                ?>
            </select>
        </div>

        <div class = "birthday">
            <label class = "required">Ngày sinh</label>
            <input type="text1" name="birthday" placeholder="dd/mm/yyyy">
        </div>

        <div class = "Address">
            <label >Địa chỉ</label>
            <input type="text2" name = "address">
        </div>

        <div class = "image_input">
            <label >Hình ảnh</label>
            <input type="file" id="photo" name="photo" accept="image/png, image/jpeg" onchange="readURL(this);">
        </div>
        <input class="img_encode" name="img_encode" style="display: none;" />
        <input type="submit" value="Đăng ký"/></td>
    </form>
    </div>
</body>

<?php
?>
</html>
