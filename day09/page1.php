<?php
$q1 = array(0 => 'PHP: Hypertext Preprocessor', 1 => 'Personal Hypertext Processor', 2 => 'Private Home Page', 3 => 'Pass');
$q2 = array(0 => 'echo "Hello World";', 1 => 'Document.Write("Hello World");', 2 => 'print("Hello World");', 3 => '"Hello World";');
$q3 = array(0 => '$', 1 => '&', 2 => '!', 3 => '%');
$q4 = array(0 => 'Pear and C', 1 => 'VBScript', 2 => 'Javascript', 3 => 'Java');
$q5 = array(0 => 'Request.Form;', 1 => 'Request.QueryString;', 2 => '$_GET[];', 3 => '$_GET');
?>
<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    for ($i = 0; $i < 5; $i++) {
        setcookie("$i", $_POST["$i"], time() + 3600, '/');
    }
    header("location: page2.php");
}
?>
<!DOCTYPE html>
<html>

<body>

    <h1>Test PHP</h1>

    <form action="" method="POST" enctype="multipart/form-data">
        <p>1. What does PHP stand for?</p>
        <?php
        for ($i = 0; $i < count($q1); $i++) {
            echo "<input type=\"radio\" name = \"0\"  value=\"$i\">{$q1[$i]}<br>";
        }
        ?>

        <p>2. How do you write "Hello World" in PHP</p>
        <?php
        for ($i = 0; $i < count($q2); $i++) {
            echo "<input type=\"radio\" name = \"1\"  value=\"$i\">{$q2[$i]}<br>";
        }
        ?>

        <p>3. All variables in PHP start with which symbol?</p>
        <?php
        for ($i = 0; $i < count($q3); $i++) {
            echo "<input type=\"radio\" name = \"2\"  value=\"$i\">{$q3[$i]}<br>";
        }
        ?>

        <p>4. The PHP syntax is most similar to:</p>
        <?php
        for ($i = 0; $i < count($q4); $i++) {
            echo "<input type=\"radio\" name = \"3\"  value=\"$i\">{$q4[$i]}<br>";
        }
        ?>

        <p>5. How do you get information from a form that is submitted using the "get" method?</p>
        <?php
        for ($i = 0; $i < count($q5); $i++) {
            echo "<input type=\"radio\" name = \"4\"  value=\"$i\">{$q5[$i]}<br>";
        }
        ?>

        <br>

        <input type="submit" value="Next">
    </form>

</body>

</html>