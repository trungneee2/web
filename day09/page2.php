<?php
$q6 = array(0 => 'open("time.txt");', 1 => 'fopen("time.txt","r+");', 2 => 'fopen("time.txt","r");', 3 => 'open("time.txt","read");');
$q7 = array(0 => '$GLOBALS;', 1 => '$_SERVER', 2 => '$_GET', 3 => '$_SESSION');
$q8 = array(0 => '++count', 1 => '$count++;', 2 => 'count++', 3 => '$count =+1');
$q9 = array(0 => 'setcookie()', 1 => 'createcookie', 2 => 'makecookie()', 3 => 'createcookie()');
$q10 = array(0 => '===', 1 => '==', 2 => '=', 3 => '!==');
?>
<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    for ($i = 5; $i < 10; $i++) {
        setcookie("$i", $_POST["$i"], time() + 3600, '/');
    }
    header("location: page3.php");
}
?>
<!DOCTYPE html>
<html>

<body>

    <h1>Test PHP</h1>

    <form action="" method="POST" enctype="multipart/form-data">
        <p>6. What is the correct way to open the file "time.txt" as readable?</p>
        <?php
        for ($i = 0; $i < count($q6); $i++) {
            echo "<input type=\"radio\" name = \"5\"  value=\"$i\">{$q6[$i]}<br>";
        }
        ?>

        <p>7. Which superglobal variable holds information about headers, paths, and script locations?</p>
        <?php
        for ($i = 0; $i < count($q7); $i++) {
            echo "<input type=\"radio\" name = \"6\"  value=\"$i\">{$q7[$i]}<br>";
        }
        ?>

        <p>8. What is the correct way to add 1 to the $count variable?</p>
        <?php
        for ($i = 0; $i < count($q8); $i++) {
            echo "<input type=\"radio\" name = \"7\"  value=\"$i\">{$q8[$i]}<br>";
        }
        ?>

        <p>9 How do you create a cookie in PHP?</p>
        <?php
        for ($i = 0; $i < count($q9); $i++) {
            echo "<input type=\"radio\" name = \"8\"  value=\"$i\">{$q9[$i]}<br>";
        }
        ?>

        <p>10. Which operator is used to check if two values are equal and of same data type?</p>
        <?php
        for ($i = 0; $i < count($q10); $i++) {
            echo "<input type=\"radio\" name = \"9\"  value=\"$i\">{$q10[$i]}<br>";
        }
        ?>

        <br>

        <input type="submit" value="Nộp bài">
    </form>

</body>

</html>